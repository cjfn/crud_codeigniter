<?php

class Empleado extends CI_controller
{
	function __construct()// creo constructor
	{
		parent::__construct();
		$this->load->model('Model_Empleado');//extendo a modelo
	}

		public function index()
		{
			$data['contenido']="Empleado/index";
			$data['selPuestos']=$this->Model_Empleado->selPuestos();//traigo todos los registros de perfil
			$data['listEmpleado']=$this->Model_Empleado->listEmpleado();
			$this->load->view("plantilla",$data);
		}

		public function insert()
		{
			$datos=$this->input->post();

			if(isset($datos))
			{

				$Nombre = $datos['nombre'];
				$Apellido = $datos['apellido'];
				$Direccion = $datos['direccion'];
				$Telefono = $datos['telefono'];
				$Puesto = $datos['puesto'];
				$DPI = $datos['dpi'];
				$Fecha = $datos['fecha'];
				$this->Model_Empleado->insertEmpleado($Nombre,$Apellido,$Direccion,$Telefono,$Puesto,$DPI,$Fecha);
				redirect('');
				
			}
		}

		public function delete($id=NULL)
		{
			if($id!=NULL)
			{
				$this->Model_Empleado->deleteEmpleado($id); 
				redirect('');
						
				
			}
		}
		
		public function edit($id)
		{
			if($id!=NULL)
			{
				$data['contenido']='Empleado/edit';
				$this->load->helper('form');
				$data['selPuestos']=$this->Model_Empleado->selPuestos();
				$data['datosEmpleado']=$this->Model_Empleado->editEmpleado($id);
				$this->load->view('plantilla',$data);
			}
			else
			{
				redirect('');
			}
		}

		public function update(){
        $datos = $this->input->post();        
        if(isset($datos)){
            $idempleado = $datos['idempleado'];
            $nombre = $datos['nombre'];
            $apellido = $datos['apellido'];
            $direccion = $datos['direccion'];
            $telefono = $datos['telefono'];
            $idPuesto = $datos['idPuesto'];
            $dpi = $datos['dpi'];
            $fecha = $datos['fecha'];

            $this->Model_Empleado->updateEmpleado($idempleado,$nombre, $apellido, $direccion, $telefono, $idPuesto,$dpi,$fecha);
            redirect('');
        }
    }

	
}