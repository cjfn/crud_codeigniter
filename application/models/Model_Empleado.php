<?php
class Model_Empleado extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// funcion de select * en sql

	public function selPuestos()
	{
		$query = $this->db->query("Select * from puestos");
		return $query->result();
	}

	//funcion para insertar empleado

	public function insertEmpleado($Nombre, $Apellido, $Direccion, $Telefono, $Puesto, $DPI, $Fecha)
	{
		$arrayDatos = array(
			'Nombre'=>$Nombre,
			'Apellido'=>$Apellido,
			'Direccion'=>$Direccion,
			'Telefono'=>$Telefono,
			'idPuesto'=>$Puesto,
			'DPI'=>$DPI,
			'FechaNacimiento'=>$Fecha
			);

		$this->db->insert('empleados', $arrayDatos);
	}

	public function listEmpleado()
	{
		$query=$this->db->query('SELECT * FROM empleados as e inner join puestos as p on e.idPuesto=p.idPuesto');
		return $query->result();
	}

	public function deleteEmpleado($id)
	{
		$this->db->where('idEmpleado', $id);
		$this->db->delete('empleados');


	}

	public function editEmpleado($id)
	{

		$query=$this->db->query("SELECT * FROM empleados as e inner join puestos as p on e.idPuesto=p.idPuesto where e.idEmpleado = $id");
		return $query->result();
	}

	public function updateEmpleado($idempleado,$nombre,$apellido, $direccion, $telefono, $idPuesto, $dpi,$fecha){
        $array = array(
            'idEmpleado' => $idempleado,
            'Nombre' => $nombre,
            'Apellido' => $apellido,
            'Direccion' => $direccion,
            'Telefono' => $telefono,            
            'idPuesto' => $idPuesto,
            'DPI' => $dpi,
            'FechaNacimiento' => $fecha        
        );
        $this->db->where('idEmpleado', $idempleado);
        $this->db->update('empleados',$array);
    }
    
}