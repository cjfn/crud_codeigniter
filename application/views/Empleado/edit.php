<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <form method="POST" action="<?php echo base_url('Empleado/update')?>">
            <?php foreach ($datosEmpleado as $value) { ?>
                
            <input type="hidden" name="idempleado" value="<?php echo $value->idEmpleado; ?>">
            <div class="form-group">
              <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">NOMBRE</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="nombre"  value="<?php echo $value->Nombre;?>">
        </div>
      </div>
      <div class="form-group">
        <label for="apellido" class="col-sm-2 control-label">APELLIDO</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="apellido"  value="<?php echo $value->Apellido;?>">
        </div>
      </div>
      <div class="form-group">
        <label for="direccion" class="col-sm-2 control-label">DIRECCION</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="direccion" p value="<?php echo $value->Direccion;?>">
        </div>
      </div>
      <div class="form-group">
        <label for="telefono" class="col-sm-2 control-label">TELEFONO</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="telefono"  value="<?php echo $value->Telefono;?>">
        </div>
      </div>
      <div class="form-group">
        <label for="puesto" class="col-sm-2 control-label">PUESTO</label>
        <div class="col-sm-10">
          <?php 
                  $lista = array();
                  foreach ($selPuestos as $registro) { 
                      $lista[$registro->idPuesto] = $registro->Puesto;
                  } 
                  
                  echo form_dropdown('idPuesto',$lista,$value->idPuesto, 'class="form-control"');
                  ?>
        </div>
      </div>
      <div class="form-group">
        <label for="dpi" class="col-sm-2 control-label">DPI</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="dpi"  value="<?php echo $value->DPI;?>">
        </div>
      </div>
      <div class="form-group">
        <label for="fecha" class="col-sm-2 control-label">FECHA DE NACIMIENTO</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" name="fecha"  value="<?php echo $value->FechaNacimiento;?>">
        </div>
      </div>
      <?php } ?>
            <button type="submit" class="btn btn-default">Actualizar Usuario</button>
          </form>
    </div>
    
</div>