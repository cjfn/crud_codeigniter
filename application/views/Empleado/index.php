
<div class="container">


  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">REGISTRO</a></li>
    <li><a data-toggle="tab" href="#menu1">CONSULTA</a></li>
     <li><a data-toggle="tab" href="#menu2">INFORMACION</a></li>

  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>REGISTRO</h3>
      
<!--aqui estara el crud de empleados-->
<h1> FORMULARIO DE REGISTRO</h1>
<form method="POST" action="<?php echo base_url('Empleado/insert')?>">
  <div class="form-group">
    <label for="nombre" class="col-sm-2 control-label">NOMBRE</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="nombre" placeholder="nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="apellido" class="col-sm-2 control-label">APELLIDO</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="apellido" placeholder="apellido">
    </div>
  </div>
  <div class="form-group">
    <label for="direccion" class="col-sm-2 control-label">DIRECCION</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="direccion" placeholder="direccion">
    </div>
  </div>
  <div class="form-group">
    <label for="telefono" class="col-sm-2 control-label">TELEFONO</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="telefono" placeholder="telefono">
    </div>
  </div>
  <div class="form-group">
    <label for="puesto" class="col-sm-2 control-label">PUESTO</label>
    <div class="col-sm-10">
      <select name="puesto" class="form-control">
      <?php foreach($selPuestos as $value)
      {?>
        <option value="<?php echo $value->idPuesto?>"><?php echo $value->Puesto?></option>  
      <?php } ?>
      
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="dpi" class="col-sm-2 control-label">DPI</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="dpi" placeholder="dpi">
    </div>
  </div>
  <div class="form-group">
    <label for="fecha" class="col-sm-2 control-label">FECHA DE NACIMIENTO</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" name="fecha" placeholder="fecha">
    </div>
  </div>
  
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Ingreso</button>
    </div>
  </div>
</form>

    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>LISTADO DE EMPLEADOS</h3>
     <table class="table table-bordered">
    <tdead>
      <tr>
        <td>cod empleado </td>
        <td>nombre</td>
        <td>apellido</td>
        <td>direccion</td>
        <td>telefono </td>
        <td>puesto </td>
        <td>dpi</td>
        <td>fecha de nacimiento </td>
        <td>fecha de ingreso</td>
         <td>ACCIONES </td>
      </tr>
    </thead>
    <tbody>
      <tr>
      <?php foreach ($listEmpleado as $key => $value) {?>
       <td><?php echo $value->idEmpleado;?></td>
       <td><?php echo $value->Nombre;?></td>
       <td><?php echo $value->Apellido;?></td>
       <td><?php echo $value->Direccion;?></td>
       <td><?php echo $value->Telefono;?></td>
       <td><?php echo $value->Puesto;?></td>
       <td><?php echo $value->DPI;?></td>
       <td><?php echo $value->FechaNacimiento;?></td>
       <td><?php echo $value->FechaIngresoRegistro;?></td>
       <td>
         <a href="<?php echo base_url('Empleado/delete')."/".$value->idEmpleado;?>" class="btn btn-warning"> eliminar</a>
         <a href="<?php echo base_url('Empleado/edit')."/".$id=$value->idEmpleado;?>" class="btn btn-danger"> Editar</a>
       </td>

     </tr>
     </tbody>
     <?php  
     }

     ?>
     </table>

    </div>

      <div id="menu2" class="tab-pane fade">
      <h3>CONTACTO</h3>
        <BR/>
        <BR/>
        <BR/>
        <CENTER> Creado por </CENTER>
        <center><strong>José Florian</strong></center>
        <center><strong>cjfn10101@gmail.com</strong></center>
    </div>
    
</div>




